const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        res.data = UserService.getUsers();
    }
    catch (err) {
        res.err = {
            code: 500,
            message: "Sorry, something happened wrong"
        }
    }
    finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = UserService.getUser(req.params.id);
    }
    catch (err) {
        res.err = {
            code: 404,
            message: err.message
        }
    }
    finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {

    if(!res.err) {
        try {
            res.data = UserService.createUser(req.body);
        }
        catch (err) {
            res.err = {
                code: 404,
                message: err.message
            }
        }
    }

    next();

}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {

    if(!res.err) {
        try {
            res.data = UserService.updateUser(req.params.id, req.body);
        }
        catch (err) {
            res.err = {
                code: 404,
                message: err.message
            }
        }
    }

    next();

}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        UserService.deleteUser(req.params.id);
    }
    catch (err) {
        res.err = {
            code: 404,
            message: err.message
        }
    }
    finally {
        next();
    }
}, responseMiddleware);

module.exports = router;