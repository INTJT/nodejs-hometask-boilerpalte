const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        res.data = FighterService.getFighters();
    }
    catch (err) {
        res.err = {
            code: 500,
            message: "Sorry, something happened wrong"
        }
    }
    finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = FighterService.getFighter(req.params.id);
    }
    catch (err) {
        res.err = {
            code: 404,
            message: err.message
        }
    }
    finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {

    if(!res.err) {
        try {
            res.data = FighterService.createFighter(req.body);
        }
        catch (err) {
            res.err = {
                code: 404,
                message: err.message
            }
        }
    }

    next();

}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    
    if(!res.err) {
        try {
            res.data = FighterService.updateFighter(req.params.id, req.body);
        }
        catch (err) {
            res.err = {
                code: 404,
                message: err.message
            }
        }
    }

    next();

}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        FighterService.deleteFighter(req.params.id);
    }
    catch (err) {
        res.err = {
            code: 404,
            message: err.message
        }
    }
    finally {
        next();
    } 
}, responseMiddleware);

module.exports = router;