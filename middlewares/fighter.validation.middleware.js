const { fighter } = require('../models/fighter');

const fighterProperties = new Set(Object.keys(fighter));
fighterProperties.delete("id");

const createFighterValid = (req, res, next) => {
    
    let {
        id,
        name,
        health,
        power,
        defense
    } = req.body;

    if(!health) {
        health = req.body["health"] = 100;
    }

    if(
        !id &&
        name &&
        power &&
        defense &&
        Object.keys(req.body).length === fighterProperties.size
    ) {

        if(typeof(power) !== "number" || power < 1 || power > 100) {
            res.err = {
                code: 400,
                message: "Power must be a number in range(1, 100)"
            }
        }

        if(typeof(defense) !== "number" || defense < 1 || defense > 10) {
            res.err = {
                code: 400,
                message: "Defense must be a number in range(1, 10)"
            }
        }

        if(typeof(health) !== "number" || health < 80 || health > 120) {
            res.err = {
                code: 400,
                message: "Health must be a number in range(80, 120)"
            }
        }

    }
    else{
        res.err = {
            code: 400,
            message: "Bad body properties"
        }
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    let properties = Object.keys(req.body);

    for(property of properties) {
        if(!fighterProperties.has(property)) {
            res.err = {
                code: 400,
                message: `Bad body property: ${property}`
            }
            next();
            return;
        }
    }

    if(properties.length === 0) {
        res.err = {
            code: 400,
            message: "At least one property"
        }
    }

    let { power, defense, health } = req.body;

    if(power){
        if(typeof(power) !== "number" || power < 1 || power > 100) {
            res.err = {
                code: 400,
                message: "Power must be a number in range(1, 100)"
            }
        }
    }

    if(defense) {
        if(typeof(defense) !== "number" || defense < 1 || defense > 10) {
            res.err = {
                code: 400,
                message: "Defense must be a number in range(1, 10)"
            }
        }
    }

    if(health) {
        if(typeof(health) !== "number" || health < 80 || health > 120) {
            res.err = {
                code: 400,
                message: "Health must be a number in range(80, 120)"
            }
        }
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;