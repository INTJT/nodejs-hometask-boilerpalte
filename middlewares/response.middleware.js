const responseMiddleware = (req, res, next) => {
    if (res.err) {
        res.status(res.err.code);
        res.send(JSON.stringify({
            error: true,
            message: res.err.message
        }));
    }
    else {
        res.status(200);
        res.send(JSON.stringify(res.data || {}));
    }
}

exports.responseMiddleware = responseMiddleware;