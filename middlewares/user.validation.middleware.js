const { user } = require('../models/user');

const userProperties = new Set(Object.keys(user));
userProperties.delete("id");

const createUserValid = (req, res, next) => {
    let {
        id,
        email, 
        password, 
        firstName, 
        lastName, 
        phoneNumber
    } = req.body;

    if(
        !id &&
        email &&
        password &&
        firstName &&
        lastName &&
        phoneNumber &&
        Object.keys(req.body).length === userProperties.size
    ) {

        if(!/@gmail.com$/.test(email)) {
            res.err = {
                code: 400,
                message: "Just google"
            }
        }

        if(!/\+380\d{9}/.test(phoneNumber)) {
            res.err = {
                code: 400,
                message: "Only ukrainians numbers"
            }
        }

        if(password.length < 3) {
            res.err = {
                code: 400,
                message: "Minimum length of password is 3 symbols"
            }
        }

    }
    else{
        res.err = {
            code: 400,
            message: "Bad body properties"
        }
    }

    next();
}

const updateUserValid = (req, res, next) => {
    let properties = Object.keys(req.body);

    for(property of properties) {
        if(!userProperties.has(property)) {
            res.err = {
                code: 400,
                message: `Bad body property: ${property}`
            }
            next();
            return;
        }
    }

    if(properties.length === 0) {
        res.err = {
            code: 400,
            message: "At least one property"
        }
    }

    if(req.body.email) {
        if(!/@gmail.com$/.test(req.body.email)) {
            res.err = {
                code: 400,
                message: "Just google"
            }
        }
    }

    if(req.body.phoneNumber) {
        if(!/\+380\d{9}/.test(req.body.phoneNumber)) {
            res.err = {
                code: 400,
                message: "Only ukrainians numbers"
            }
        }    
    }

    if(req.body.password) {
        if(req.body.password.length < 3) {
            res.err = {
                code: 400,
                message: "Minimum length of password is 3 symbols"
            }
        }
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;