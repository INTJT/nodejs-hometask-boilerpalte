const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    exitstsFighter(id) {
        return Boolean(FighterRepository.getOne({ id }));
    }

    getFighters() {
        return FighterRepository.getAll();
    }

    getFighter(id) {
        const item = FighterRepository.getOne({ id });
        if(!item) {
            throw new Error("Fighter not found");
        }
        return item;
    }

    createFighter(fighterData) {

        if(this.search(fighter => fighter.name.toLowerCase() === fighterData.name.toLowerCase())) {
            throw new Error(`Fighter ${fighterData.name} already exists`);
        }

        return FighterRepository.create(fighterData);

    }

    updateFighter(id, fighterData) {
        if(!this.exitstsFighter(id)) {
            throw new Error("Fighter not found");
        }
        else {
            return FighterRepository.update(id, fighterData);
        }
    }

    deleteFighter(id) {
        if(!this.exitstsFighter(id)) {
            throw new Error("Fighter not found");
        }
        else {
            FighterRepository.delete(id);
        }
    }

}

module.exports = new FighterService();