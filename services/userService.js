const { UserRepository } = require('../repositories/userRepository');

class UserService {

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    existsUser(id) {
        return Boolean(UserRepository.getOne({ id }));
    }

    getUser(id) {
        const item = UserRepository.getOne({ id });
        if(!item) {
            throw new Error("User not found");
        }
        return item;
    }

    getUsers() {
        return UserRepository.getAll();
    }

    createUser(userData) {

        if(this.search(user => user.email.toLowerCase() === userData.email.toLowerCase())) {
            throw new Error(`Email ${userData.email} is already used`);
        }

        if(this.search({ phoneNumber: userData.phoneNumber })) {
            throw new Error(`PhoneNumber ${userData.phoneNumber} is already used`);
        }

        return UserRepository.create(userData);

    }

    updateUser(id, userData) {
        if(!this.existsUser(id)) {
            throw new Error("User not found");
        }
        else {
            return UserRepository.update(id, userData);
        }
    }

    deleteUser(id) {
        if(!this.existsUser(id)) {
            throw new Error("User not found");
        }
        else {
            UserRepository.delete(id);
        }
    }

}

module.exports = new UserService();